#!/bin/bash

alias django="python manage.py"

function app
{
    echo "running in [app] mode"
    echo "Migrating db..."
    python manage.py migrate
    echo "Preparing static files"
    python manage.py collectstatic --noinput
    echo "Starting app..."
    gunicorn overfitting.wsgi:application -w 2 -b :8000
}


function usage
{
    echo "usage: entrypoints.sh [app|worker]"
}

if [ "$1" != "" ]; then
    case $1 in
        app | application )     app
                                ;;
        worker )                worker
                                ;;
        -h | --help )           usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
else
    echo "Please specify a param:"
    usage
fi