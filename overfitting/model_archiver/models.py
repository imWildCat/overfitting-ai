import uuid
from django.db import models

# Create your models here.


class Item(models.Model):
    HIDDEN = 0
    NORMAL = 1
    STATUSES = (
        (HIDDEN, 'hidden'),
        (NORMAL, 'normal'),
    )

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    title = models.CharField(max_length=255)
    subtitle = models.CharField(max_length=255, blank=True, null=True)
    content = models.TextField(blank=True, null=True)
    reply_count = models.PositiveIntegerField(default=0)
    hit_count = models.PositiveIntegerField(default=0)
    status = models.PositiveSmallIntegerField(choices=STATUSES, default=1)
    homepage_url = models.CharField(max_length=511, blank=True, null=True)
    download_url = models.CharField(max_length=511, blank=True, null=True)

    created_at = models.DateTimeField(auto_now_add=True, db_index=True)
    updated_at = models.DateTimeField(auto_now=True)

    list_display = ('title')

    def __str__(self):
        return self.title
