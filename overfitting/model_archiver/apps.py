from django.apps import AppConfig


class ModelArchiverConfig(AppConfig):
    name = 'model_archiver'
