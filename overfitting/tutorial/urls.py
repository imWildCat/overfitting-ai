from django.conf.urls import url, include

from .apis import router

urlpatterns = [
    url(r'^', include(router.urls)),
]
