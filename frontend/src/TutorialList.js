import './App.css';
import { } from 'antd';

import React, { Component } from 'react';

import {getTutorials} from './networking';

class TutorialList extends Component {

  state = {
    data: null,
  }

  render() {
    return (
        <div>
           {this.state.data ? this.renderList(): 'loading...'}
        </div>
    );
  }

  renderList() {
    return (
      <div className="list">
        {this.state.data.map(r => this.renderRow(r))}
      </div>
    );
  }

  renderRow(rowData) {
    const {title, subtitle, url} = rowData;
    return (
      <div className="row" style={{marginBottom: 20}}>
        <h2><a target="_blank" href={url}>{title}</a></h2>
        <p>{subtitle}</p>
      </div>
    )
  }

  async componentDidMount() {
    const data = await getTutorials();
    this.setState({data: data.data});
  }
}

export default TutorialList;
