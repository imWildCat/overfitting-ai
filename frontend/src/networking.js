import axios from 'axios';

const ENV = process.env.NODE_ENV;

function buildURL(uri) {
    return uri;
}

export function getModels() {
    return axios.get(buildURL('/api/v1/model/items'));
}

export function getTutorials() {
    return axios.get(buildURL('/api/v1/tutorial/items/'));
}