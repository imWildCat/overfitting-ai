import './App.css';
import { } from 'antd';

import React, { Component } from 'react';

import { getModels } from './networking';

class ModelList extends Component {

  state = {
    data: null,
  }

  render() {
    if (this.state.data) {
      return this.renderList();
    } else {
      return (
        <div>
          Loading...
        </div>
      );
    }
  }

  renderList() {

    return (
      <div>
        {this.state.data.map(r => this.renderRow(r))}

      </div>
    );
  }

  renderRow(rowData) {
    const { title, subtitle, download_url, homepage_url } = rowData;
    return (
      <div key={title} style={{ borderBottom: '1px solid #DEDEDE', marginBottom: 25 }}>
        <h2>
          {title}
        </h2>

        <p>{subtitle ? `${subtitle}` : null}</p>

        <p>
          {homepage_url ? <a href={homepage_url} target="_blank">Homepage</a> : null}
          {download_url ? <a href={download_url} target="_blank" style={{ marginLeft: 5 }}>Download</a> : null}
        </p>
      </div>
    );
  }

  async componentDidMount() {
    const data = await getModels();
    console.log(data.data);
    this.setState({ data: data.data, });
  }
}

export default ModelList;
