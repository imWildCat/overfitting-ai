import './App.css';

import { Layout, LocaleProvider, Menu } from 'antd';
import React, { Component } from 'react';

import CreditList from './CreditList';
import ModelList from './ModelList';
import TutorialList from './TutorialList';
import enUS from 'antd/lib/locale-provider/en_US';

const { Header, Footer, Content, } = Layout;


class App extends Component {

  state = {
    tabIndex: 1,
  }

  onMenuClick({ item, key, keyPath }) {
    console.log({ item, key, keyPath });
    this.setState({ tabIndex: Number(key) });
  }

  renderContent(index) {
    switch (index) {
      case 1:
        return <TutorialList />;
      case 2:
        return <ModelList />;
      case 3:
      default:
        return <CreditList />;
    }
  }

  render() {
    return (
      <LocaleProvider locale={enUS}>
        <Layout className="layout">
          <Header>
            <div className="logo" style={
              { float: 'left', fontSize: 18, color: 'white', marginRight: 10, }
            }>Overfitting.ai</div>
            <Menu
              theme="dark"
              mode="horizontal"
              onClick={this.onMenuClick.bind(this)}
              defaultSelectedKeys={['1']}
              style={{ lineHeight: '64px', float: 'left' }}
            >
              <Menu.Item key="1">Tutorials</Menu.Item>
              <Menu.Item key="2">Models</Menu.Item>
           
            </Menu>
          </Header>
          <Content style={{ padding: '0 50px', marginTop: 20, }}>

            <div style={{ background: '#fff', padding: 24, minHeight: 280 }}>
                {this.renderContent(this.state.tabIndex)}
            </div>
          </Content>
          <Footer style={{ textAlign: 'center' }}>
            Overfitting.ai © 2017
         </Footer>
        </Layout>
      </LocaleProvider>
    );
  }
}

export default App;
