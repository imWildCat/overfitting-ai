default:
	echo "Hello"

build:
	make build_app && make build_frontend
build_app:
	docker build -t "overfitting-app" ./overfitting
build_frontend:
	docker build -t "overfitting-nginx" -f Dockerfile-nginx .
upgrade:
	docker-compose stop
	docker-compose rm -f
	make build